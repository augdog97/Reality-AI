/**
 * 1. Installed the express package via "npm install express --save-dev"
 *  - Express is the backend to serve the production build of the site.
 * 2. (a )Requiring the express package and setting the value to express.
 * 3. (b) Creating a new express instance and setting it to the variable "app"
 * 4. (c) serving the static files such as, CSS, Images, and JS
 * 5. (d) Making a get request using the express wildcard to render the index.html file.
 * 6. (e) Telling express to listen and serve the app on port 8080.
 *  - This is the port used for which the host server is listening.
 *  - (e.1) Logging a message on the port being used to give an indication that the server is running.
 */


/*(a)*/const express = require('express');

/* (b) */ const app = express();

/* c */ app.use(express.static('./dist/Reality-AI-App'));

 /* (d) */ app.get('/*', (req, res) =>
     res.sendFile('index.html', { root: 'dist/Reality-AI-App/' }),
 );

 /* (e) */app.listen(process.env.PORT || 8080);
   /* (e.1) */ console.log('This app is listening on port: 8080');