import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Created the Nasa API Service to handle the Http request to the api.
 * 1. (a) Created the NasaStation interface to describe the expected type of response returned from the Nasa API.
 * 2. (b) Used the injectable decorator so the service can be injected using DI (Dependency Injection)
 *       - (b.1) injected the HttpClient into the constructor using DI (Dependency Injection).
 *       - (b.2) Created a function getNasaData() with type of an Observable of <NasaStation> our interface.
 *          - By indicating this type, we indicate the type of the response wrapped inside the Observable.
 *          - We will return this Observable in the service and the Nasa component will be the consumer of the 
 *           Observable. 
 * 
 */




// (a)
export interface NasaStation {
  name: string;
  nametype: string;
  id: string;
  year: string;
  recclass: string;
}

// (b)
@Injectable()
export class NasaApiService {
  // (b.1)
  constructor(private _http: HttpClient) {

  }
  // (b.2)
  getNasaData(): Observable<NasaStation> {
    return this._http.get<NasaStation>('https://data.nasa.gov/resource/gh4g-9sfh.json');
  }
}
