import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

//Services
import { NasaApiService } from './nasa-api.service';


// Using 'f' to only run this unit test
fdescribe('NasaApiService', () => {
  let service: NasaApiService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
    // Importing modules, provide services, and declar components. Angular needs to know what artifacts of the app to get for testing.
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule  //immported the HTTP testing module provided by angular
      ],
      providers: [
        NasaApiService
      ],
      declarations: []
    }).compileComponents();
    service = TestBed.get(NasaApiService);
    httpMock = TestBed.get(HttpTestingController); //set a mock up that listens to outgoing API calls. 
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the nasa api service', () => {
    service.getNasaData().subscribe((res) => {  //called the function and subscribed to the response. 
      //do nothing with response
    });

    const req = httpMock.expectOne('https://data.nasa.gov/resource/gh4g-9sfh.json'); //expected one HTTP call 

    expect(req.request.method).toEqual('GET'); //verified the type of request. GET
    expect(req.request.url).toEqual('https://data.nasa.gov/resource/gh4g-9sfh.json'); //Verified the URL of the request. 

    req.flush({}); //mock the URL response with a flush and returned an empty object. 
  });
});
