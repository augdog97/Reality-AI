// Core Imports
import { Component, OnInit, ViewChild } from '@angular/core';

// Material Angular imports
import { MatTable } from '@angular/material/table';
import { MatSort } from '@angular/material/sort'
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';



// Service Import
import { NasaApiService } from '../nasa-api.service';

/**
 * 1. (a) Declaring data variable to be used for API data
 * 2. (b) Declaring dataSource to be used by Material Angular
 * 3. (c) Declaring DisplayedColums which is of type an array of strings and holds the column names to be used by Material Angular Table
 * 4. (d) Starting implmentation of sorting of the table. 
 *      - set static to false because the table is dynamic. 
 *      - set sort variable to have type of MatSort
 * 5. (d.1) Starting implementation for Pagination using MatPaginator.
 * 6. (e) injected the NasaApiService into the constructor
 * 7. (f) subscribing to the Observable from the NasaApiService with variable data
 *      - (f.1) setting the return value from the api to the "data" variable. 
 *      - (f.2) Setting the data returned from the api into this.dataSource so Material Angular table can display, and sort data. 
 *      - (f.3) Finishing implementation of sort feature.
 *      - (f.4) Implementing the Pagination feature. 
 *      - (f.5) Logging the API data to the console. 
 * 8. (g) implementing the filter function with value being of type string. 
 */

@Component({
  selector: 'app-nasa',
  templateUrl: './nasa.component.html',
  styleUrls: ['./nasa.component.css']
})
export class NasaComponent implements OnInit {
  // (a)
  data;

  // (b)
  dataSource;

  // (c)
  displayedColumns: string[] = ['name', 'nametype', 'id', 'year', 'recclass'];

  // (d)
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  // (d.1)
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // (e)
  constructor(private _nasa: NasaApiService) {

  }

  ngOnInit() {
    // (f)
    this._nasa.getNasaData().subscribe(data => {
      // (f.1)
      this.data = data;
      // (f.2)
      this.dataSource = new MatTableDataSource(this.data);
      // (f.3)
      this.dataSource.sort = this.sort;
      //(f.4)
      this.dataSource.paginator = this.paginator;
      // (f.5)
      console.log(this.data);


    })

  }
  // (g)
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

}
