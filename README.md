This is a simple app that makes a request to a NASA api renders the data in a table and to be able to sort and filter that data. 



To run production build of app. 
1. Clone Repo
2. run npm install
3. Run "node server.js or npm start"
4. Visit localhost:8080

# You can find the link to the project below:
https://reality-ai.herokuapp.com/

# Features
1. Render API data in Material Angular Table
2. Sort the table by header
3. Filter table function via the "filter" input
4. Pagination: This is how showing 10 rows is accomplished.
5. Unit testing for Nasa API service
6. CI/CD pipeline 


# RealityAIApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
